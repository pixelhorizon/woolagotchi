﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Microsoft.WindowsAzure.MobileServices;

namespace Woolagotchi.Droid
{
	
	[Activity (Label = "Woolagotchi.Droid", Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			global::Xamarin.Forms.Forms.Init (this, bundle);

			LoadApplication (new App());
		}

		public static MobileServiceClient MobileService = new MobileServiceClient(
			"https://woolagotchi.azure-mobile.net/",
			"QkwiWuqULijyVwMKwSurvDqnuqvBMh30"
		);
	}


}

