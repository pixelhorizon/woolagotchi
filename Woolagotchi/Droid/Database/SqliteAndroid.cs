﻿using System;
using Woolagotchi.Data;
using System.IO;
using SQLite.Net;
using Xamarin.Forms;
using Woolagotchi.Droid;

//TODO show IOC vs Dependency Injection
[assembly: Dependency (typeof (SqliteAndroid))]

namespace Woolagotchi.Droid
{
	public class SqliteAndroid : ISqlite
	{
		#region ISqlite implementation

		public SQLiteConnection GetConnection ()
		{
			const string sqliteFilename = "database.db3";
			var documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
			var path = Path.Combine (documentsPath, sqliteFilename);
			var plat = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid();

			//Create the connection
			var conn = new SQLiteConnection(plat,path);

			return conn;
		}

		#endregion



		public SqliteAndroid ()
		{
		}
	}
}

