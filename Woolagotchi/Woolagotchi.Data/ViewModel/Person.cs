﻿using System;
using SQLite.Net;
using SQLite.Net.Attributes;
using System.Linq;
using System.Collections.Generic;

namespace Woolagotchi.Data
{
	public class Person
	{

		public string id { get; set; }
		[PrimaryKey, AutoIncrement]
		public int userID{ get; set; }
		public string firstName { get; set; }
		public string lastName { get; set; }
		public string userName { get; set; }
		public string hashedPassword { get; set; }
		public string userSalt { get; set; }
		public int coins { get; set; }
		[Ignore]
		public List<Items> myItems { get; set; }
		[Ignore]
		public Items myEquippedItem { get; set; }


		public Person (string xUserName,string xSalt, string xPassword)
		{
			userName = xUserName;
			hashedPassword = xPassword; //password is not hashed yet
			userSalt = xSalt;

			myItems = new List<Items>();

			coins = 0;


		}

		public Person (string xfirstName,string xlastName)
		{
			firstName = xfirstName;
			lastName = xlastName;

			myItems = new List<Items>();


			coins = 0;
		}

		public Person ()
		{
			myItems = new List<Items>();


			coins = 0;
		}

		public void addItem(Items thisItem)
		{
			myItems.Add (thisItem);
		}

		public void updateItem(Items thisItem) {
			for (int i = 0; i < myItems.Count; i++) {
				if (myItems [i].itemID == thisItem.itemID) {
					thisItem.itemQuantity += myItems [i].itemQuantity;
					myItems [i] = thisItem; // Set the position of the item to the new item with new quantity
				}
			}
		}

		public bool hasItem(Items thisItem) {
			foreach (Items i in myItems) {
				if (i.itemID == thisItem.itemID)
					return true;
			}
			return false;
		}

		public override string ToString()
		{
			return string.Format("[Person: ID={0}, FirstName={1}, LastName={2}]", userID, firstName, lastName);
		}

		public string fullName()
		{
			return this.firstName + ' ' + this.lastName;
		}
	}
}

