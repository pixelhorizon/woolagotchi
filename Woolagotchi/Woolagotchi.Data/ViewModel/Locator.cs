﻿using System;
using Microsoft.Practices.ServiceLocation;
using GalaSoft.MvvmLight.Ioc;
using Woolagotchi.Data.ViewModel;

namespace Woolagotchi.Data
{
	public class Locator
	{
		/// <summary>
		/// Register all the used ViewModels, Services et. al. witht the IoC Container
		/// </summary>
		public Locator()
		{
			ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

			// ViewModels
			SimpleIoc.Default.Register<MainViewModel>();

			// Services
			SimpleIoc.Default.Register<IPeopleService, PeopleServiceStub>();
		}

		/// <summary>
		/// Gets the Main property.
		/// </summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
			"CA1822:MarkMembersAsStatic",
			Justification = "This non-static member is needed for data binding purposes.")]
		public MainViewModel Main
		{
			get { return ServiceLocator.Current.GetInstance<MainViewModel>(); }
		}
	}
}

