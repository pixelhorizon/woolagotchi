using GalaSoft.MvvmLight;
using System.Collections.ObjectModel;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;



namespace Woolagotchi.Data.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
		/// 
		private readonly IPeopleService _peopleService;
		ObservableCollection<Person> People { get; set; }

		public MainViewModel(IPeopleService peopleService)
        {

			if (peopleService == null) throw new ArgumentNullException("peopleService");
			_peopleService = peopleService;

          
        }

		public async Task Init()
		{
			if (People != null) return;

			People = new ObservableCollection<Person>(await _peopleService.GetPeople());
		}


    }

	public interface IPeopleService
	{
		Task<IEnumerable<Person>> GetPeople();
	}

	public class PeopleServiceStub:IPeopleService
	{
		public Task<IEnumerable<Person>> GetPeople()
		{
			const int numberOfPeopleToGenerate = 100;
			return Task.Run(() => GeneratePeople(numberOfPeopleToGenerate));
		}

		private IEnumerable<Person> GeneratePeople(int personCount)
		{
			var people = new List<Person>(personCount);

			for (int i = 0; i < personCount; ++i)
			{
				people.Add(new Person(NameGenerator.GenRandomFirstName(), NameGenerator.GenRandomLastName()));
			}

			return people;
		}
	}
}