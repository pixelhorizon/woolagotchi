﻿using System;
using SQLite.Net.Attributes;

namespace Woolagotchi.Data
{
	public class Barcode
	{
		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public string barcode { get; set; }
		public int pointValue { get; set; }

		public Barcode ()
		{
			
		}

		public Barcode (int xID, string xBarcode, int xPointValue) {
			ID = xID;
			barcode = xBarcode;
			pointValue = xPointValue;
		}
	}
}

