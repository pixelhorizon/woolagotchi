﻿using System;
using System.Windows.Input;
using System.Collections.ObjectModel;
using Microsoft.WindowsAzure.MobileServices;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Linq;
using System.Diagnostics;

namespace Woolagotchi.Data
{
	public class TestViewModel : BaseViewModel
	{
		MobileServiceClient mobileServiceClient;

		private ObservableCollection<Item> itemList {get;set;} //value

		private ObservableCollection<User> personList {get;set;} //value

		public ObservableCollection<Item> ItemList { //accessors
			get { return itemList; } 
			set {
				if (value != null) {
					itemList = value;
					RaisePropertyChanged (() => ItemList);
				}
				;
			}
		}

		public ObservableCollection<User> PersonList { //accessors
			get { return personList; } 
			set {
				if (value != null) {
					personList = value;
					RaisePropertyChanged (() => PersonList);
				}
				;
			}
		}

		public ICommand editItem{ get; private set; }

		public ICommand loadItems{ get; private set; }

		public ICommand addItem{ get; private set; }

		public ICommand addCoins{ get; private set; }

		public ICommand loadPeople{ get; private set; }

		public ICommand addUser{ get; private set; }

		public ICommand changeUser{ get; private set; }

		private string text { get; set; }

		public string TextValue { 
			get { return text; } 
			set {
				if (value != null) {
					text = value;
					RaisePropertyChanged (() => TextValue);
				}
			}
		}



		public TestViewModel ()
		{
			mobileServiceClient = new MobileServiceClient ("https://woolagotchi.azure-mobile.net/", "QkwiWuqULijyVwMKwSurvDqnuqvBMh30");

			loadItems = new Command (async () => {

				ItemList = await LoadItems();

				//PeopleList.Add (new Person{ firstName = UserName });
			});

			loadPeople = new Command (async () => {

				PersonList = await LoadPeople();

			});

			addUser = new Command (async () => {

				IMobileServiceTable<User> itemTable = mobileServiceClient.GetTable<User>();

				User newUser = new User();
				newUser.userName = TextValue;
				newUser.hashedPassword = "";
				newUser.userSalt = "";

				await itemTable.InsertAsync(newUser);

				PersonList = await LoadPeople();

			});

			addItem = new Command (async () => {

				IMobileServiceTable<Item> itemTable = mobileServiceClient.GetTable<Item>();

				Item thisItem = new Item();
				thisItem.Text = TextValue;

				await itemTable.InsertAsync(thisItem);

				ItemList = await LoadItems();

				//PeopleList.Add (new Person{ firstName = UserName });
			});

			changeUser = new Command (async () => {

				IMobileServiceTable<User> personTable = mobileServiceClient.GetTable<User>();

				List<User> thesePeople = await personTable.ToListAsync ();

				List<User> thisItem = thesePeople.Where(table => table.userName == "Test").ToList();

				Debug.WriteLine("Records: " + thisItem.Count.ToString());

				User anotherItem = thisItem.First();

				anotherItem.userName = TextValue;

				Debug.WriteLine("Name: " + anotherItem.userName);
				Debug.WriteLine("id: " + anotherItem.id);

				await personTable.UpdateAsync(anotherItem);

				Debug.WriteLine("Name: " + anotherItem.userName);
				Debug.WriteLine("id: " + anotherItem.id);

				PersonList = await LoadPeople();
			});

			editItem = new Command (async () => {

				IMobileServiceTable<Item> itemTable = mobileServiceClient.GetTable<Item>();

				List<Item> theseItems = await itemTable.ToListAsync();
				List<Item> thisItem = theseItems.Where(table => table.Text == "Test").ToList();

				Item anotherItem = thisItem.First();

				anotherItem.Text = TextValue;

				Debug.WriteLine("Text: " + anotherItem.Text);

				await itemTable.UpdateAsync(anotherItem);

				Debug.WriteLine("Text: " + anotherItem.Text);

				ItemList = await LoadItems();

				//PeopleList.Add (new Person{ firstName = UserName });
			});

			addCoins = new Command (async () => {

				IMobileServiceTable<User> personTable = mobileServiceClient.GetTable<User>();

				List<User> People = await personTable.ToListAsync();
				List<User> Adens = People.Where(table => table.userName == "AdenJones").ToList();

				User Aden = Adens.First();

				Aden.coins = 100;

				await personTable.UpdateAsync(Aden);

				PersonList = await LoadPeople();

			});
		}

		async Task<ObservableCollection<Item>> LoadItems()
		{
			IMobileServiceTable<Item> itemTable = mobileServiceClient.GetTable<Item>();

			List<Item> theseItems = await itemTable.ToListAsync ();

			ObservableCollection<Item> newList = new ObservableCollection<Item>();

			foreach( Item it in theseItems)
			{
				newList.Add(it);
			}


			return newList;

		}

		async Task<User> UpdateUser()
		{
			IMobileServiceTable<User> personTable = mobileServiceClient.GetTable<User>();

			List<User> thesePeople = await personTable.ToListAsync ();

			List<User> thisItem = thesePeople.Where(table => table.userName == "Test").ToList();

			User anotherItem = thisItem.First();


			anotherItem.userName = TextValue;

			await personTable.UpdateAsync(anotherItem);

			return anotherItem;

		}

		async Task<ObservableCollection<User>> LoadPeople()
		{
			IMobileServiceTable<User> personTable = mobileServiceClient.GetTable<User>();

			List<User> thesePeople = await personTable.ToListAsync ();

			ObservableCollection<User> newList = new ObservableCollection<User>();

			foreach( User it in thesePeople)
			{
				newList.Add(it);
			}


			return newList;

		}


	}
}

