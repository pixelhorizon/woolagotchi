﻿using System;
using GalaSoft.MvvmLight;
namespace Woolagotchi.Data
{
	public class BaseViewModel : ViewModelBase
	{
		public string LoggedInUsersCoins {
			get { return Settings.loggedInUser.coins.ToString() + "W"; } 
			set {
				if (value != null) {
					int myInt;

					if (int.TryParse (value, out myInt)) {
						Settings.loggedInUser.coins = myInt;
						RaisePropertyChanged (() => LoggedInUsersCoins);
					}
				}
			}
		}

		public Items UserEquippedItem {
			get { return Settings.loggedInUser.myEquippedItem; } 
			set {
				if (value != null) {
					Settings.loggedInUser.myEquippedItem = value;
					RaisePropertyChanged (() => UserEquippedItem);
				}
			}
		}

		public BaseViewModel ()
		{
			
			// Get Coin value if user is logged in and set to zero if user has no coins else users coin total

//			if (Woolagotchi.Data.Settings.loggedInUser != null) {
//				if (Woolagotchi.Data.Settings.loggedInUser.coins > 0) {
//					LoggedInUsersCoins = Woolagotchi.Data.Settings.loggedInUser.coins.ToString();
//				} else {
//					LoggedInUsersCoins = "0";
//				}
//			}

			// Get ItemEquipped if user is logged in and set to blank if user has no equipped item
			if (Woolagotchi.Data.Settings.loggedInUser != null) {
				if (Woolagotchi.Data.Settings.loggedInUser.myEquippedItem != null) {
					UserEquippedItem = Woolagotchi.Data.Settings.loggedInUser.myEquippedItem;
				} else {
					// set user item to be 'blank item'
					UserEquippedItem = Settings.blankItem;
				}
			}

		}
	}
}

