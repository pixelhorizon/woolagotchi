using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using System.Diagnostics;
using System;

namespace Woolagotchi.Data.ViewModel
{
	public class ViewModelLocator
	{
		public const string HomePageKey = "HomePage";
		public const string StartPageKey = "StartPage";
		public const string LogInPageKey = "LoginPage";

		static ViewModelLocator()
		{
			ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

			SimpleIoc.Default.Register<SignUpViewModel> ();

			SimpleIoc.Default.Register<LoginViewModel>(() => 
				{
					return new LoginViewModel(
						SimpleIoc.Default.GetInstance<IMyNavigationService>()
					);
				});
			SimpleIoc.Default.Register<HomeViewModel> ();
			SimpleIoc.Default.Register<InventoryViewModel> ();
			SimpleIoc.Default.Register<ShopViewModel> ();
			SimpleIoc.Default.Register<ScanViewModel> ();
			SimpleIoc.Default.Register<RedeemViewModel> ();
			SimpleIoc.Default.Register<ShareViewModel> ();
			SimpleIoc.Default.Register<TestViewModel> ();
			SimpleIoc.Default.Register<LogOutViewModel>(() => 
				{
					return new LogOutViewModel(
						SimpleIoc.Default.GetInstance<IMyNavigationService>()
					);
				});
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
			"CA1822:MarkMembersAsStatic",
			Justification = "This non-static member is needed for data binding purposes.")]
		public SignUpViewModel SignUp
		{
			get
			{
				return ServiceLocator.Current.GetInstance<SignUpViewModel> ();
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
			"CA1822:MarkMembersAsStatic",
			Justification = "This non-static member is needed for data binding purposes.")]
		public LoginViewModel LogIn
		{
			get
			{
				return ServiceLocator.Current.GetInstance<LoginViewModel> ();
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
			"CA1822:MarkMembersAsStatic",
			Justification = "This non-static member is needed for data binding purposes.")]
		public HomeViewModel Home
		{
			get
			{
				return ServiceLocator.Current.GetInstance<HomeViewModel> ();
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
			"CA1822:MarkMembersAsStatic",
			Justification = "This non-static member is needed for data binding purposes.")]
		public InventoryViewModel Inventory {
			get
			{
				return ServiceLocator.Current.GetInstance<InventoryViewModel> ();
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
			"CA1822:MarkMembersAsStatic",
			Justification = "This non-static member is needed for data binding purposes.")]
		public ShopViewModel Shop {
			get
			{
				return ServiceLocator.Current.GetInstance<ShopViewModel> ();
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
			"CA1822:MarkMembersAsStatic",
			Justification = "This non-static member is needed for data binding purposes.")]
		public ScanViewModel Scan {
			get
			{
				return ServiceLocator.Current.GetInstance<ScanViewModel> ();
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
			"CA1822:MarkMembersAsStatic",
			Justification = "This non-static member is needed for data binding purposes.")]
		public RedeemViewModel Redeem {
			get
			{
				return ServiceLocator.Current.GetInstance<RedeemViewModel> ();
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
			"CA1822:MarkMembersAsStatic",
			Justification = "This non-static member is needed for data binding purposes.")]
		public ShareViewModel Share {
			get
			{
				return ServiceLocator.Current.GetInstance<ShareViewModel> ();
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
			"CA1822:MarkMembersAsStatic",
			Justification = "This non-static member is needed for data binding purposes.")]
		public TestViewModel Test {
			get
			{
				return ServiceLocator.Current.GetInstance<TestViewModel> ();
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
			"CA1822:MarkMembersAsStatic",
			Justification = "This non-static member is needed for data binding purposes.")]
		public LogOutViewModel LogOut {
			get
			{
				return ServiceLocator.Current.GetInstance<LogOutViewModel> ();
			}
		}
	}
}