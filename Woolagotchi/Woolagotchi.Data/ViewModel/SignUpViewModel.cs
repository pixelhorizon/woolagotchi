﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using System.Windows.Input;
using Xamarin.Forms;
using Woolagotchi.Data.ViewModel;
using Microsoft.WindowsAzure.MobileServices;
using System.Collections.Generic;
using System.Diagnostics;

namespace Woolagotchi.Data
{
	public class SignUpViewModel : ViewModelBase
	{
		private IMyNavigationService navigationService;

		private ObservableCollection<Person> peopleList {get;set;} //value

		public ICommand SignUp{ get; private set; }

		public ObservableCollection<Person> PeopleList { //accessors
			get { return peopleList; } 
			set {
				if (value != null) {
					peopleList = value;
					RaisePropertyChanged (() => PeopleList);
				}
				;
			}
		}

		private string userName { get; set; }

		public string UserName { 
			get { return userName; } 
			set {
				if (value != null) {
					userName = value;
					RaisePropertyChanged (() => UserName);
				}
			}
		}

		private string hashedPassword{get;set;}

		public string HashedPassword { 
			get { return hashedPassword; } 
			set {
				if (value != null) {
					hashedPassword = value;
					RaisePropertyChanged (() => HashedPassword);
				}
			}
		}

		private string userNamePrompt{get;set;}

		public string UserNamePrompt{ 
			get { return userNamePrompt; } 
			set {
				if (value != null) {
					userNamePrompt = value;
					RaisePropertyChanged (() => UserNamePrompt);
				}
			}
		}

		private Color userNameBGC{get;set;}

		public Color UserNameBGC{ 
			get { return userNameBGC; } 
			set {
				if (value != null) {
					userNameBGC = value;
					RaisePropertyChanged (() => UserNameBGC);
				}
			}
		}

		private Color userNameTextColor{ get; set; }

		public Color UserNameTextColor{
			get { return userNameTextColor; } 
			set {
				if (value != null) {
					userNameTextColor = value;
					RaisePropertyChanged (() => UserNameTextColor);
				}
			}
		}

		public SignUpViewModel(IMyNavigationService navigationService){
			//PeopleList = new ObservableCollection<Person>();

			//var database1 = new Database1 ();
			var azureDB = new AzureDB();
			//PeopleList = new ObservableCollection<Person> (database1.GetAll ());

			UserNamePrompt = "User Name";
			UserNameBGC = Color.Gray;

			this.navigationService = navigationService;

			SignUp = new Command (async () => {


				string Salt = GeneralFunctions.getRandomString(24);

				string thisHash = GeneralFunctions.createHash(HashedPassword,Salt);

				Person thisPerson = new Person(UserName,Salt,thisHash);

				//bool isDuplicate = true;

				bool isDuplicate = await azureDB.CheckDuplicateUserName(thisPerson);

				if(!isDuplicate)
				{
					//database1.InsertOrUpdatePerson(thisPerson);
					await azureDB.AddPerson(thisPerson);

					UserNameBGC = Color.Transparent;

					Person thisUser = await azureDB.getPersonByUserName(UserName);

					Woolagotchi.Data.Settings.loggedInUser = thisUser;

					Debug.WriteLine(thisUser.id.ToString());

					this.navigationService = navigationService;
					this.navigationService.NavigateTo (ViewModelLocator.HomePageKey);

					/*
					List<Person> AllPeople = await azureDB.GetPeople();

					ObservableCollection<Person> newList = new ObservableCollection<Person>();

					foreach( Person pers in AllPeople)
					{
						newList.Add(pers);
					}

					PeopleList = newList;
					*/

				}
				else
				{
					UserName = "";
					UserNamePrompt = "User Name already taken!";
					UserNameBGC = Color.Red;
					UserNameTextColor = Color.White;
				}

				//PeopleList.Add (new Person{ firstName = UserName });
			});
		}


	}
}

