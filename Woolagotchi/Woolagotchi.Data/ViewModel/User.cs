﻿using System;

namespace Woolagotchi.Data
{
	public class User
	{
		public string id { get; set; }
		public string userName { get; set; }
		public string hashedPassword { get; set; }
		public string userSalt { get; set; }
		public int coins { get; set; }

	}
}

