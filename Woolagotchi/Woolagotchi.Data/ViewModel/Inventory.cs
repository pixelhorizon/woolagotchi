﻿using System;
using SQLite.Net;
using SQLite.Net.Attributes;

namespace Woolagotchi.Data
{
	public class Inventory
	{
		[PrimaryKey][AutoIncrement]
		public int inventoryID { get; set; }
		public int userID { get; set; }
		public int itemID { get; set; }
		public int quantity { get; set; }
		
		public Inventory ()
		{
		}

		public Inventory (int thisUser, int thisItem, int thisQuantity) {
			userID = thisUser;
			itemID = thisItem;
			quantity = thisQuantity;
		}

		public override string ToString()
		{
			return string.Format("[PersonID={1}, ItemID={2}, Quantity={3}]", userID, itemID, quantity);
		}
	}
}

