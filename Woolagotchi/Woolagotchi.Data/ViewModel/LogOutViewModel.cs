﻿using System;
using GalaSoft.MvvmLight;
using System.Windows.Input;
using Xamarin.Forms;
using Woolagotchi.Data.ViewModel;

namespace Woolagotchi.Data
{
	public class LogOutViewModel : ViewModelBase
	{
		public ICommand LogOut{ get; private set; }

		public LogOutViewModel (IMyNavigationService navigationService)
		{

			LogOut = new Command (async () => {
				Settings.loggedInUser = null;
				navigationService.NavigateTo(ViewModelLocator.StartPageKey);
			});
		}
	}
}

