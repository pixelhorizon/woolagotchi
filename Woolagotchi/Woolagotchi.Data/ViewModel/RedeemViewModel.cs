using System;
using GalaSoft.MvvmLight;
using System.Windows.Input;
using Xamarin.Forms;
using Microsoft.WindowsAzure.MobileServices;
using System.Diagnostics;

namespace Woolagotchi.Data
{
	public class RedeemViewModel : BaseViewModel
	{
		MobileServiceClient mobileServiceClient;

		public ICommand AddCoins{ get; private set; }

		private string coins { get; set; }

		public string Coins { 
			get { return coins; } 
			set {
				if (value != null) {
					coins = value;
					RaisePropertyChanged (() => Coins);
				}
			}
		}

		private string currentCoins { get; set; }

		public string CurrentCoins { 
			get { return currentCoins; } 
			set {
				if (value != null) {
					currentCoins = value;
					RaisePropertyChanged (() => CurrentCoins);
				}
			}
		}

		private string userID { get; set; }

		public string UserID { 
			get { return userID; } 
			set {
				if (value != null) {
					userID = value;
					RaisePropertyChanged (() => UserID);
				}
			}
		}

		public RedeemViewModel ()
		{
			mobileServiceClient = new MobileServiceClient ("https://woolagotchi.azure-mobile.net/", "QkwiWuqULijyVwMKwSurvDqnuqvBMh30");

			CurrentCoins = Woolagotchi.Data.Settings.loggedInUser.coins.ToString();
			UserID = Woolagotchi.Data.Settings.loggedInUser.id.ToString ();

			var azureDB = new AzureDB();

			AddCoins = new Command (async() => {

				int theseCoins;

				if( !int.TryParse(Coins, out theseCoins))
				{
					//do nothing

				} else {
					IMobileServiceTable<Person> persTable =	mobileServiceClient.GetTable<Person>();

					Person thisPerson = Woolagotchi.Data.Settings.loggedInUser;

					thisPerson.coins += theseCoins;

					Debug.WriteLine("Coins: " + thisPerson.coins.ToString());

					//await persTable.UpdateAsync(thisPerson);

					Debug.WriteLine("Coins: " + thisPerson.coins.ToString());

					CurrentCoins = thisPerson.coins.ToString();

					//Person newPerson = await azureDB.AddCoinsToUser( Woolagotchi.Data.Settings.loggedInUser,theseCoins);
					//Woolagotchi.Data.Settings.loggedInUser = newPerson;

					//Woolagotchi.Data.Settings.loggedInUser.coins = theseCoins;

					//CurrentCoins = newPerson.coins.ToString();
					//CurrentCoins = theseCoins.ToString();
				}

			});
		}
	}
}