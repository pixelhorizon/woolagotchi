﻿using System;
using GalaSoft.MvvmLight;
using Xamarin.Forms;
using System.Windows.Input;
using Woolagotchi.Data.ViewModel;

namespace Woolagotchi.Data
{
	public class LoginViewModel : ViewModelBase
	{
		private IMyNavigationService navigationService;

		public ICommand LogMeIn{ get; private set; }

		private string userName { get; set; }

		public string UserName {
			get { return userName; }
			set {
				if (value != null) {
					userName = value;
					RaisePropertyChanged (() => UserName);
				}
			}
		}

		private string hashedPassword{get;set;}

		public string HashedPassword {
			get { return hashedPassword; }
			set {
				if (value != null) {
					hashedPassword = value;
					RaisePropertyChanged (() => HashedPassword);
				}
			}
		}


		private string userNamePrompt{get;set;}

		public string UserNamePrompt{
			get { return userNamePrompt; }
			set {
				if (value != null) {
					userNamePrompt = value;
					RaisePropertyChanged (() => UserNamePrompt);
				}
			}
		}

		private Color userNameBGC{get;set;}

		public Color UserNameBGC{
			get { return userNameBGC; }
			set {
				if (value != null) {
					userNameBGC = value;
					RaisePropertyChanged (() => UserNameBGC);
				}
			}
		}

		private Color userNameTextColor{ get; set; }

		public Color UserNameTextColor{
			get { return userNameTextColor; }
			set {
				if (value != null) {
					userNameTextColor = value;
					RaisePropertyChanged (() => UserNameTextColor);
				}
			}
		}

		public LoginViewModel (IMyNavigationService navigationService)
		{
			//var database1 = new Database1 ();
			var azureDB = new AzureDB();

			UserNamePrompt = "Enter User Name";
			UserNameBGC = Color.Gray;

			this.navigationService = navigationService;

			LogMeIn = new Command (async() => {

				bool isValid = await azureDB.ValidateUser(UserName,HashedPassword);

				if( isValid == false)
				{
					//bad login
					UserName = "";
					UserNamePrompt = "Bad Username Password combo!";
					UserNameBGC = Color.Red;
					UserNameTextColor = Color.Black;
				} else {

					Person thisUser = await azureDB.getPersonByUserName(UserName);

					Woolagotchi.Data.Settings.loggedInUser = thisUser;
					this.navigationService = navigationService;
					this.navigationService.NavigateTo (ViewModelLocator.HomePageKey);

				}

			});

			/*
			var database1 = new Database1 ();

			UserNamePrompt = "Enter User Name";
			UserNameBGC = Color.Transparent;

			LogMeIn = new Command (() => {

				if( database1.ValidateUser(UserName,HashedPassword) == false)
				{
					//bad login
					UserName = "";
					UserNamePrompt = "Bad Username Password combo!";
					UserNameBGC = Color.Red;
					UserNameTextColor = Color.Black;
				} else {

					this.navigationService = navigationService;

					this.navigationService.NavigateTo (ViewModelLocator.HomePageKey);
					//MessagingCenter.Send<LoginViewModel, Page>(this, "PushPage", new HomePage());
				}

			});
			*/
		}
	}
}

