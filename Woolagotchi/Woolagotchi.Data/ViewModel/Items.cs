﻿using System;
using SQLite.Net.Attributes;

namespace Woolagotchi.Data
{
	public class Items
	{
		[PrimaryKey, AutoIncrement]
		public int itemID { get; set; }
		public string itemName { get; set; }
		public int itemPrice { get; set; }
		public string itemDescription { get; set; }
		public string itemImage { get; set; }
		public int itemQuantity { get; set; }

		public Items (int xItemID, string xItemName, int xItemPrice, string xItemDescription, string xItemImage)
		{
			itemID = xItemID;
			itemName = xItemName;
			itemPrice = xItemPrice;
			itemDescription = xItemDescription;
			itemImage = xItemImage;
			itemQuantity = 1;
		}

		public Items (int xItemID, string xItemName, int xItemPrice, string xItemDescription, string xItemImage, int xItemQuantity)
		{
			itemID = xItemID;
			itemName = xItemName;
			itemPrice = xItemPrice;
			itemDescription = xItemDescription;
			itemImage = xItemImage;
			itemQuantity = xItemQuantity;
		}

		public Items() {
		}

		public override string ToString()
		{
			return string.Format("[Item: ID={0}, Name={1}, Price={2}, Image={3}, Description={4}, Quantity={5}]", itemID, itemName, itemPrice, itemImage, itemDescription, itemQuantity);
		}
	}
}

