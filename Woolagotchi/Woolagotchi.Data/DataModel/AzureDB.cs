﻿using System;
using Microsoft.WindowsAzure.MobileServices;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Woolagotchi.Data
{
	public class AzureDB
	{
		MobileServiceClient mobileServiceClient;


		public AzureDB ()
		{
			mobileServiceClient = new MobileServiceClient ("https://woolagotchi.azure-mobile.net/", "QkwiWuqULijyVwMKwSurvDqnuqvBMh30");

		}

		async public Task<bool> CheckDuplicateUserName(Person thisPerson)
		{
			IMobileServiceTable<Person> persTable =	mobileServiceClient.GetTable<Person>();

			string UserName = thisPerson.userName;
			//bool rtValue = false;

			//try{
			List<Person> People = await persTable.ToListAsync();
			List<Person> person = People.Where(table => table.userName == UserName).ToList();
			/*}
			catch( Exception ex ) {
				Debug.WriteLine (ex.Message);
			}finally{

				rtValue = true;

			}
			*/
			//return rtValue;

			return person.Count > 0;

				

		}

		async public Task<bool> ValidateUser(string username, string password)
		{

			IMobileServiceTable<Person> persTable =	mobileServiceClient.GetTable<Person>();

			List<Person> People = await persTable.ToListAsync();
			List<Person> person = People.Where(table => table.userName == username).ToList();


			//var thisUser = database.Table<Person> ().Where (x => x.userName == username).ToList<Person>();

			if (person.Any() == false) {
				return false;
			} else {

				Person User = person.First();

				if (User.hashedPassword == GeneralFunctions.createHash (password, User.userSalt)) {
					return true;
				} else {
					return false;
				}

			}

		}

		async public Task<bool> AddPerson(Person thisPerson)
		{
			IMobileServiceTable<Person> persTable =	mobileServiceClient.GetTable<Person>();

			await persTable.InsertAsync(thisPerson);

			return true;
		}

		async public Task<Person> getPersonByUserName(string Username)
		{
			IMobileServiceTable<Person> persTable =	mobileServiceClient.GetTable<Person>();

			List<Person> People = await persTable.ToListAsync();
			List<Person> person = People.Where(table => table.userName == Username).ToList();

			return person.First ();

		}

		async public Task<List<Person>> GetPeople()
		{
			IMobileServiceTable<Person> persTable =	mobileServiceClient.GetTable<Person>();

			List<Person> People = await persTable.ToListAsync();

			return People;
		}

		async public Task<Person> AddCoinsToUser(Person User,int Coins)
		{
			IMobileServiceTable<Person> persTable =	mobileServiceClient.GetTable<Person>();

			List<Person> People = await persTable.ToListAsync();
			List<Person> person = People.Where(table => table.userName == User.userName).ToList();

			Person thisUser = person.First ();

			thisUser.coins += Coins;

			await persTable.UpdateAsync (thisUser);

			People = await persTable.ToListAsync();
			person = People.Where(table => table.userName == User.userName).ToList();

			thisUser = person.First ();

			// returns user so you can update user with new coins value
			return thisUser;

		}

	}
}

