﻿using System;
using System.Diagnostics;
using System.IO;
using SQLite.Net;
using Xamarin.Forms;
using System.Linq;
using System.Collections.Generic;

namespace Woolagotchi.Data
{
	public class Database1
	{
		SQLiteConnection database;

		public List<Items> items = new List<Items> {
			new Items(1, "Lollipop", 500, "Save 5% on all sweets.", "lollipop.png"), 
			new Items(2, "Banana", 500, "Save 5% on all fruits.", "banana.png"),
			new Items(3, "Hat", 500, "Save 5% on all housery.", "hat.png"),
			new Items(4, "Bread", 500, "Save 5% on all bakery.", "bread.png"),
			new Items(5, "Lightning", 10000, "Save 5% on all items.", "lightning.png"),
			new Items(6, "Can", 500, "Save 5% on all drinks.", "can.png")
		};

		public List<Barcode> barcodes = new List<Barcode> {
			new Barcode(1, "9312828059513", 245),
			new Barcode(2, "9341816005737", 550),
			new Barcode(3, "9873294832743", 784),
			new Barcode(4, "56431289468942", 850),
			new Barcode(5, "5641387867438", 1250)
		};

		public Database1 ()
		{

			database = DependencyService.Get<ISqlite> ().GetConnection ();
			//TODO show IOC vs Dependency injection
			//			database = SimpleIoc.Default.GetInstance<ISqlite> ().GetConnection ();
			if (database.TableMappings.All(t => t.MappedType.Name != typeof(Person).Name)) {
				database.CreateTable<Person> ();
				database.Commit ();
			}

			if (database.TableMappings.All (t => t.MappedType.Name != typeof(Inventory).Name)) {
				database.CreateTable<Inventory> ();
				database.Commit ();
			}

			if (database.TableMappings.All (t => t.MappedType.Name != typeof(Items).Name)) {
				database.CreateTable<Items> ();
				database.Commit ();

				foreach (Items i in items) {
					if (database.Table<Items> ().Where (x => x.itemID == i.itemID).Any ()) {
						database.Update (i);
					} else { 
						database.Insert (i);
					}
				}
			}

			if (database.TableMappings.All (t => t.MappedType.Name != typeof(Barcode).Name)) {
				database.CreateTable<Barcode> ();
				database.Commit ();

				foreach (Barcode bc in barcodes) {
					if (database.Table<Barcode> ().Where (x => x.barcode == bc.barcode).Any ()) {
						database.Update (bc);
					} else { 
						database.Insert (bc);
					}
				}
			}
		}

		public List<Person> GetAll(){
			var people = database.Table<Person> ().ToList<Person>();

			return people;
		}

		public Person GetUserByUserName(string username)
		{
			var thisUser = database.Table<Person> ().Where (x => x.userName == username).ToList<Person>();
			return thisUser.First();
		}

		public bool ValidateUser(string username, string password)
		{
			var thisUser = database.Table<Person> ().Where (x => x.userName == username).ToList<Person>();

			if (thisUser.Any() == false) {
				return false;
			} else {

				Person User = thisUser.First();

				if (User.hashedPassword == GeneralFunctions.createHash (password, User.userSalt)) {
					return true;
				} else {
					return false;
				}

			}
				
		}

		public int InsertOrUpdatePerson(Person thisPerson){
			return database.Table<Person> ().Where (x => x.userID == thisPerson.userID).Any() 
				? database.Update (thisPerson) : database.Insert (thisPerson);
		}

		public bool CheckDuplicateUserName(Person thisPerson){
			return database.Table<Person> ().Where (x => x.userName == thisPerson.userName).Any ();
		}

		/**
		 * Inventory Queries
		 **/
		public List<Inventory> GetInventoryByUserID (int userID) {
			return database.Table<Inventory> ().Where (x => x.userID == userID).ToList<Inventory> ();
		}

		public List<Inventory> GetAllInventories () {
			return database.Table<Inventory> ().ToList<Inventory>();
		}

		public int InsertOrUpdateInventoryByUserID(Inventory thisInventory){
			return database.Table<Inventory> ().Where (x => x.userID == thisInventory.userID).Any() 
				? database.Update (thisInventory) : database.Insert (thisInventory);
		}

		public int InsertOrUpdateInventory(Inventory thisInventory){
			return database.Table<Inventory> ().Where (x => x.inventoryID == thisInventory.inventoryID || 
				(x.inventoryID == thisInventory.inventoryID && x.itemID == thisInventory.itemID) || 
				(x.userID == thisInventory.userID && x.itemID == thisInventory.itemID)).Any() 
				? database.Update (thisInventory) : database.Insert (thisInventory);
		}

		/**
		 * Item Queries
		 **/
		public List<Items> GetAllItems(){
			return database.Table<Items> ().ToList<Items>();
		}

		public Items GetItemByID(int itemID) {
			return database.Table<Items> ().Where (x => x.itemID == itemID).ToList<Items> ().First();
		}

		public int InsertOrUpdateItems(Items thisItem){
			return database.Table<Items> ().Where (x => x.itemID == thisItem.itemID).Any() 
				? database.Update (thisItem) : database.Insert (thisItem);
		}

		/**
		 * Barcode Queries
		 **/
		public Barcode GetBarcode(string thisBarcode) {
			return database.Table<Barcode> ().Where (x => x.barcode == thisBarcode).ToList<Barcode> ().First ();
		}
	}
}

