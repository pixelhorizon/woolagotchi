﻿using System;
using SQLite.Net;

namespace Woolagotchi.Data
{
	public interface ISqlite
	{
		SQLiteConnection GetConnection();
	}
}

