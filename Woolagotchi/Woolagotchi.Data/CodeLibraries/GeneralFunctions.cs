﻿using System;
using PCLCrypto;
using System.Diagnostics;

namespace Woolagotchi.Data
{
	public class GeneralFunctions
	{
		public GeneralFunctions ()
		{
		}

		public static string createHash(string XPassword, string xSalt)
		{

			string hashBase64 = "";

			try{
			byte[] data = System.Text.Encoding.UTF8.GetBytes (xSalt + XPassword);

			var hasher = WinRTCrypto.HashAlgorithmProvider.OpenAlgorithm(HashAlgorithm.Sha1);
			byte[] hash = hasher.HashData(data);
			hashBase64 = Convert.ToBase64String(hash);
			}
			catch( Exception ex)
			{
				Debug.WriteLine ("And the error is: ");
				Debug.WriteLine (ex.Message);
			}

			return hashBase64;
		}

		public static string getRandomString(int length)
		{
			string validChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!@#$%^&*()-=_+[]{}`~<>,./?;:";
			string randomString = "";
			Random rnd = new Random();

			int numChars = validChars.Length;

			for (int i = 0; i < length; i++) {
				randomString += validChars [rnd.Next (1, numChars) - 1];
			}

			return randomString;
		}
	}
}

