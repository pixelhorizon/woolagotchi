﻿using System;

using Xamarin.Forms;
using Woolagotchi.Data;
using Woolagotchi.Data.ViewModel;
using GalaSoft.MvvmLight.Ioc;

namespace Woolagotchi
{
	public class App : Application
	{
		
		private static ViewModelLocator _locator;
		private static NavigationService nav;

		public static ViewModelLocator Locator
		{
			get
			{
				return _locator ?? (_locator = new ViewModelLocator());
			}
		}

		public static Page GetLogInPage()
		{
			return new LogMeIn();
		} 

		public static Page GetSignUpPage()
		{
			return new SignMeUp();
		}   

		public App ()
		{
			
			MainPage = GetMainPage();
			// The root page of your application
			//MainPage = new NavigationPage(new StartPage());




		}

		public Page GetMainPage()
		{
			nav = new NavigationService ();
			nav.Configure (ViewModelLocator.HomePageKey, typeof(HomePage));
			SimpleIoc.Default.Register<IMyNavigationService> (()=> nav, true);
			var navPage = new NavigationPage (new LogMeIn ())
			{
				BarBackgroundColor = Color.FromRgb(32, 97, 32)
			};
			nav.Initialize (navPage);
			return navPage;
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

