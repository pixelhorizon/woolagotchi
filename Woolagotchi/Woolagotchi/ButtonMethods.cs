﻿using System;
using Xamarin.Forms;

namespace Woolagotchi
{
	public class ButtonMethods
	{

		public ButtonMethods ()
		{
			
		}
		public async void AnimateImage(Image but) {
			await but.FadeTo (0.2, 50, Easing.CubicOut);
			await but.FadeTo (1, 500, Easing.CubicIn);
		}
		public async void AnimateFrame(Frame fra) {
			await fra.FadeTo (0.2, 50, Easing.CubicOut);
			await fra.FadeTo (1, 500, Easing.CubicIn);
		}
		public void Highlight(Frame theFrame, Boolean isHighlighted) {
			if (isHighlighted == true) {
				theFrame.BackgroundColor = Color.Green;
			} else {
				theFrame.BackgroundColor = Color.Transparent;
			}
		}
	}
}

