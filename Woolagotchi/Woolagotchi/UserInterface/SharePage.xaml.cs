﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Woolagotchi
{
	using System;
	using Xamarin.Forms;

	public partial class SharePage : ContentPage
	{
		public SharePage ()
		{
			InitializeComponent();
			BindingContext = App.Locator.Share;

			// Coins - in XAML

			// menu bar
			ToolbarItems.Add (new ToolbarItem {
				Text = "Scan",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new ScanPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Redeem",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new RedeemPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Home",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new HomePage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Inventory",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new InventoryPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Shop",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new ShopPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Settings",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new SettingsPage ()))
			});
			// end menu bar

			var wireButton = new ButtonMethods ();
			var tapGestureRecognizer = new TapGestureRecognizer();

			tapGestureRecognizer.Tapped += (s, e) => {
				if(s.Equals(TwitterButton)){
					wireButton.AnimateImage(TwitterButton);
					this.DisplayAlert("Twitter Button", "Uploading Wooli to Twitter...", "OK", "Cancel");
				}
				if(s.Equals(FacebookButton)){
					wireButton.AnimateImage(FacebookButton);
					this.DisplayAlert("Facebook Button", "Uploading Wooli to Facebook...", "OK", "Cancel");
				}
				if(s.Equals(GoogleButton)){
					wireButton.AnimateImage(GoogleButton);
					this.DisplayAlert("Google Button", "Uploading Wooli to Google +...", "OK", "Cancel");
				}
				if(s.Equals(InstagramButton)){
					wireButton.AnimateImage(InstagramButton);
					this.DisplayAlert("Instagram Button", "Uploading Wooli to Instagram...", "OK", "Cancel");
				}
			};

			TwitterButton.GestureRecognizers.Add(tapGestureRecognizer);
			FacebookButton.GestureRecognizers.Add(tapGestureRecognizer);
			GoogleButton.GestureRecognizers.Add(tapGestureRecognizer);
			InstagramButton.GestureRecognizers.Add(tapGestureRecognizer);
		}
	}
}

