﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Woolagotchi
{
	public partial class StartPage : ContentPage
	{
		public StartPage ()
		{
			InitializeComponent ();
		}

		protected void GoToLogIn(object sender, EventArgs args)
		{
			Navigation.PushAsync(new LogMeIn());

		}

		protected void GoToSignUp(object sender, EventArgs args)
		{
			Navigation.PushAsync(new SignMeUp());

		}

		protected void Tester(object sender, EventArgs args)
		{
			Navigation.PushAsync(new TestMVVM());

		}
	}


}

