﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Woolagotchi
{
	public partial class LogOut : ContentPage
	{
		public LogOut ()
		{
			InitializeComponent ();
			BindingContext = App.Locator.LogOut;
		}
	}
}

