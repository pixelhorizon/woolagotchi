﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Diagnostics;

namespace Woolagotchi
{
	public partial class LogMeIn : ContentPage
	{
		public LogMeIn ()
		{
			InitializeComponent ();

			BindingContext = App.Locator.LogIn;

			NavigationPage.SetHasNavigationBar(this, true);

			ToolbarItems.Add (new ToolbarItem {
				Text = "Debug/Test",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new TestMVVM()))
			});

		}

		protected void GoToHomePage(object sender, EventArgs args)
		{
			Navigation.PushAsync(new HomePage());

		}

		protected void GoToSignUp(object sender, EventArgs args)
		{
			Navigation.PushAsync(new SignMeUp());

		}

		protected void GoToSettings(object sender, EventArgs args)
		{
			Navigation.PushAsync(new SettingsPage());

		}
	}
}

