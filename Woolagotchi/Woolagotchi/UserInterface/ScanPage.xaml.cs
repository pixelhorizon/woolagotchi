﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Xamarin.Forms;
using ZXing;
using Woolagotchi.Data;

namespace Woolagotchi
{
	public partial class ScanPage : ContentPage
	{
		public Barcode bc = null;
		
		public ScanPage ()
		{
			InitializeComponent ();
			BindingContext = App.Locator.Scan;

			// Coins - in XAML

			// menu bar
			ToolbarItems.Add (new ToolbarItem {
				Text = "Scan",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new ScanPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Redeem",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new RedeemPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Home",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new HomePage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Inventory",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new InventoryPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Shop",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new ShopPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Settings",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new SettingsPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Share",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new SharePage ()))
			});
			// end menu bar

			var database = new Database1 ();
			LoadScanner (database);
		}

		public async void LoadScanner(Database1 db) {
			var scanner = new ZXing.Mobile.MobileBarcodeScanner();
			var azureDB = new AzureDB();
			var result = await scanner.Scan();

			if (result != null) {
				try {
					bc = db.GetBarcode (result.Text);
				} catch (Exception) {
					bc = null;
				}
				if (bc != null) {
					//uncomment to add add coins functionality
					//Woolagotchi.Data.Settings.loggedInUser = await azureDB.AddCoinsToUser( Woolagotchi.Data.Settings.loggedInUser,bc.pointValue);
					Woolagotchi.Data.Settings.loggedInUser.coins += bc.pointValue;
					App.Locator.Home.LoggedInUsersCoins = Woolagotchi.Data.Settings.loggedInUser.coins.ToString();
					await this.DisplayAlert ("Success", "You have been rewarded with " + bc.pointValue + " Wooli coins.", "OK");
				} else {
					await this.DisplayAlert ("Failed", "No barcode matching that product or receipt!", "OK");
				}
				// TODO: Remove/de-activate barcode once it has been used
			}

			await Navigation.PopAsync ();
		}
	}
}

