﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Xamarin.Forms;
using System.Collections.ObjectModel;
using Woolagotchi.Data;

namespace Woolagotchi
{
	public partial class ShopPage : ContentPage
	{
		private ButtonMethods wireShopButton;
		ObservableCollection<Items> itemList;

		public ShopPage ()
		{
			InitializeComponent ();

			BindingContext = App.Locator.Shop;

			var database = new Database1 ();

			itemList = new ObservableCollection<Items>(database.GetAllItems ());

			// menu bar
			ToolbarItems.Add (new ToolbarItem {
				Text = "Scan",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new ScanPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Redeem",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new RedeemPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Home",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new HomePage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Inventory",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new InventoryPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Settings",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new SettingsPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Share",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new SharePage ()))
			});
			// end menu bar

			wireShopButton = new ButtonMethods ();

			var tapGestureRecognizer = new TapGestureRecognizer();

			tapGestureRecognizer.Tapped += async (s, e) => {
				var imageSender = (Image) s;
				var imageSource = (FileImageSource) imageSender.Source;
				wireShopButton.AnimateImage(imageSender);

				foreach (Items i in itemList) {
					if (imageSource.File == i.itemImage) {
						var alert = await DisplayAlert(i.itemName, "Would you like to buy " + i.itemName + " for " + i.itemPrice + "W" + "." + System.Environment.NewLine + i.itemDescription, "Yes", "No");

						if (alert) {
							if(Woolagotchi.Data.Settings.loggedInUser.coins >= i.itemPrice){
								if (Settings.loggedInUser.hasItem(i)) {
									Settings.loggedInUser.updateItem(i);
								} else {
									Settings.loggedInUser.addItem(i); // NEW: Adds the item they purchased to the correct list of items
								}
								Woolagotchi.Data.Settings.loggedInUser.coins -= i.itemPrice;
								App.Locator.Home.LoggedInUsersCoins = Settings.loggedInUser.coins.ToString();
								App.Locator.Shop.LoggedInUsersCoins = Settings.loggedInUser.coins.ToString();
								//App.Locator.Shop.LoggedInUsersCoins = Settings.loggedInUser.coins.ToString(); // Have to update the binding connector every time we update the loggedInUsers coins
							} else {
								await this.DisplayAlert("Insufficient Funds", "Scan a few more reciepts to get more funds!", "OK");
							}
						}
					}
				}
			};

			Grid grid = new Grid
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				RowDefinitions = 
				{
					new RowDefinition { Height = new GridLength(1, GridUnitType.Star) }
				},
				ColumnDefinitions = 
				{
					new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
					new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) }
				},
				ColumnSpacing = 0,
				RowSpacing = 0
			};

			int columnCount = itemList.Count;
			int rowCount = 0;

			foreach (Items item in itemList) {
				Image image = new Image {
					Source = item.itemImage,
					StyleId = item.itemName + "Button"
				};
				grid.Children.Add (image, columnCount % 2, rowCount);
				image.GestureRecognizers.Add (tapGestureRecognizer);

				grid.Children.Add (new Label{
					Text = item.itemPrice + " W",
					FontSize = 15,
					TextColor = Color.Black,
					XAlign = TextAlignment.End
				}, columnCount % 2, rowCount);

				columnCount++;

				if (columnCount % 2 == 0) {
					rowCount++;
				}
			}

			this.Content = grid;
		}
	}
}

