﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using Woolagotchi.Data;

namespace Woolagotchi
{
	public partial class InventoryPage : ContentPage
	{
		private ButtonMethods wireInvButton;

		private static int USER_ID = 1;

		public InventoryPage ()
		{
			InitializeComponent ();
			BindingContext = App.Locator.Inventory;

			// Coins - in XAML

			// menu bar
			ToolbarItems.Add (new ToolbarItem {
				Text = "Scan",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new ScanPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Redeem",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new RedeemPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Shop",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new ShopPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Home",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new HomePage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Settings",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new SettingsPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Share",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new SharePage ()))
			});
			// end menu bar

			wireInvButton = new ButtonMethods ();

			var tapGestureRecognizer = new TapGestureRecognizer();

			tapGestureRecognizer.Tapped += async (s, e) => {
				var imageSender = (Image) s;
				var imageSource = (FileImageSource) imageSender.Source;
				wireInvButton.AnimateImage(imageSender);

				foreach (Items i in Settings.loggedInUser.myItems) {
					if (imageSource.File == i.itemImage) {
						var alert = await DisplayAlert(i.itemName, "Would you like to equip your " + i.itemName + "." + System.Environment.NewLine + i.itemDescription, "Yes", "No");

						if (alert) {
							App.Locator.Home.UserEquippedItem = i;
						}
					}
				}
			};

			Grid grid = new Grid
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				RowDefinitions = 
				{
					new RowDefinition { Height = new GridLength(1, GridUnitType.Star) }
				},
				ColumnDefinitions = 
				{
					new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
					new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) }
				},
				ColumnSpacing = 0,
				RowSpacing = 0
			};

			int columnCount = Settings.loggedInUser.myItems.Count;
			int rowCount = 0;

			foreach (Items item in Settings.loggedInUser.myItems) {
				Image image = new Image {
					Source = item.itemImage,
					StyleId = item.itemName + "Button"
				};
				grid.Children.Add (image, columnCount % 2, rowCount);
				image.GestureRecognizers.Add (tapGestureRecognizer);
				grid.Children.Add (new Label{
					Text = "x " + item.itemQuantity,
					FontSize = 15,
					TextColor = Color.Black,
					XAlign = TextAlignment.End
				}, columnCount % 2, rowCount);
				columnCount++;

				if (columnCount % 2 == 0) {
					rowCount++;
				}
			}

			this.Content = grid;
		}

//		private void resetButtons() {
//			//wireInvButton.Highlight (LollipopButton, false);
//			LollipopButton.BackgroundColor = Color.Transparent;
//			wireInvButton.Highlight (BananaButton, false);
//			wireInvButton.Highlight (HatButton, false);
//			wireInvButton.Highlight (CanButton, false);
//			wireInvButton.Highlight (LightningButton, false);
//			wireInvButton.Highlight (BreadButton, false);
//		}
	}
}

