﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Xamarin.Forms;

namespace Woolagotchi
{
	public partial class HomePage : ContentPage
	{
		public HomePage ()
		{
			

			InitializeComponent ();
			BindingContext = App.Locator.Home;

			var wireButton = new ButtonMethods ();
			var tapGestureRecognizer = new TapGestureRecognizer();

			NameLabel.Text = "Ralph";

			// Coins - in XAML			

			// menu bar
			ToolbarItems.Add (new ToolbarItem {
				Text = "User: " + Woolagotchi.Data.Settings.loggedInUser.userName,
				Order = ToolbarItemOrder.Secondary
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Scan",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new ScanPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Redeem",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new RedeemPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Shop",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new ShopPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Inventory",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new InventoryPage ()))
			});
					
			ToolbarItems.Add (new ToolbarItem {
				Text = "Settings",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new SettingsPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Share",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new SharePage ()))
			});
			// end menu bar

			tapGestureRecognizer.Tapped += (s, e) => {

				if(s.Equals(ShareButton)){
					wireButton.AnimateImage(ShareButton);
					Navigation.PushAsync(new SharePage());
				}
				if(s.Equals(InventoryButton)){
					wireButton.AnimateImage(InventoryButton);
					Navigation.PushAsync(new InventoryPage());
				}
				if(s.Equals(RedeemButton)){
					wireButton.AnimateImage(RedeemButton);
					Navigation.PushAsync(new RedeemPage());
				}
				if(s.Equals(ScanButton)){
					wireButton.AnimateImage(ScanButton);
					Navigation.PushAsync(new ScanPage());
				}
				if(s.Equals(ShopButton)){
					wireButton.AnimateImage(ShopButton);
					Navigation.PushAsync(new ShopPage());
				}
				if(s.Equals(ItemEquippedImage)){
					wireButton.AnimateImage(ItemEquippedImage);
					if(App.Locator.Home.UserEquippedItem.itemID == 7){
						Navigation.PushAsync(new InventoryPage());
					} else {
						this.DisplayAlert(App.Locator.Home.UserEquippedItem.itemName, "Name: " + App.Locator.Home.UserEquippedItem.itemName + "." + System.Environment.NewLine + App.Locator.Home.UserEquippedItem.itemDescription, "OK");
					}
				}
			};

			ShareButton.GestureRecognizers.Add(tapGestureRecognizer);
			ShopButton.GestureRecognizers.Add(tapGestureRecognizer);
			InventoryButton.GestureRecognizers.Add(tapGestureRecognizer);
			ScanButton.GestureRecognizers.Add(tapGestureRecognizer);
			RedeemButton.GestureRecognizers.Add(tapGestureRecognizer);
			ItemEquippedImage.GestureRecognizers.Add (tapGestureRecognizer);
		}
	}
}

