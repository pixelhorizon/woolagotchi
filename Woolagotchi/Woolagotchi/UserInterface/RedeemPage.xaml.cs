using System;
using System.Collections.Generic;
using ZXing;
using Xamarin.Forms;

namespace Woolagotchi
{
	public partial class RedeemPage : ContentPage
	{
		public RedeemPage ()
		{
			InitializeComponent ();
			BindingContext = App.Locator.Redeem;

			// menu bar
			ToolbarItems.Add (new ToolbarItem {
				Text = "Scan",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new ScanPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Inventory",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new InventoryPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Shop",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new ShopPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Home",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new HomePage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Settings",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new SettingsPage ()))
			});

			ToolbarItems.Add (new ToolbarItem {
				Text = "Share",
				Order = ToolbarItemOrder.Secondary,
				Command = new Command (() => Navigation.PushAsync (new SharePage ()))
			});
			// end menu bar

			if (App.Locator.Home.UserEquippedItem.itemID == 7) {
				Description.IsVisible = false;
				redeemImage.IsVisible = false;
			} else {
				Description.IsVisible = true;
				redeemImage.IsVisible = true;
			}
		}
	}
}

