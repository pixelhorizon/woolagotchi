﻿using System;
using Woolagotchi.Data.ViewModel;
using Xamarin.Forms;

namespace Woolagotchi.Data
{
	public class App
	{
		private static ViewModelLocator _locator;

		public static ViewModelLocator Locator
		{
			get
			{
				return _locator ?? (_locator = new ViewModelLocator());
			}
		}


		public static Page GetSignUpPage()
		{
			return new SignMeUp();
		}   
	}
}

